## Pre-condition:
This code can be run anywhere, but we require you to utilize Intellij to run the code. Intellij is provided on the lab computer. If you are not familiar with using Intellij on the lab computers, follow the instructions on the Foswiki page provided to boot Intellij on the lab computers or use the following link to download Intellij on your personal computer. However, we base all our performance metrics on the Lab computers so we cannot guarantee the same performance on personal devices so we recommend using the lab computers.

Link to Download: https://www.jetbrains.com/idea/download/?section=windows

## Supporting files
### How to use Project:
- W and S will move all the objects in the Z-direction
- The arrow keys will rotate the camera
- You can use the mouse to draw a line that when released will shoot the shapes in the given XY direction
- To test the game feature, the green obstacles are obstacles that will disappear upon collision and will increment the score label in the top right corner by 5.
- At the bottom of the Main Menu, you can set game game obstacles up to 100 obstacles which will be the green ones.
- You are also able to set normal obstacles which will be the orange ones which you can also set up to 100 of.
- To test out other objects, replace the src/teapot.txt file in the parameter given in part 3 with any of the other txt files under src or use one of the options provided under figure 1, 2, or 3.
- Alternatively, you can use the select file button to pick a file from your file system.
- Once you have chosen all your options, press Start Simulation.

### Execution:
- Within Intellij, go to File > New > Project From Version Control if you have an open project, or if you are opening Intellij for the first time, press the Get From VCS button
- From our gitlab repository, press the blue code button and copy the URL under clone with HTTPS
- Once you clone the code, you may receive the following pop-up. Press Load Maven Project. This makes sure that the JavaFX library is properly loaded. If you do not get the pop up, you must right click pom.xml go down to maven and click reload project. 
- Next, we want to ensure you are running this with Java JDK 17 or later
  - If you do not have Java JDK 17, here is the link to download the newest JDK: https://www.oracle.com/java/technologies/downloads/
  - If you are on the lab computer, run the following command in your terminal:
  source /ad/eng/opt/java/add_jdk17.sh
- Go to File > Project Structure > Project Settings > Project, under JDK select the proper JDK from wherever you have downloaded the JDK, then press apply and OK.
  - For lab computers, this is under /ad/eng/opt/java/jdk17_35/jdk-17
- Under your project files navigate to src > main > java > org.example.newmat > MainScene
- At the top of your screen, press the three dots next to play and debug button and press run with parameters. On the lab computer, press on the current file drop down and press edit configurations.
  - If this is the first time you are using this, you will get a Add new run configuration screen and click Application
    - Then click the browse button on the Main class and click the correct file you want to run, in this case MainScene.
  - Ensure that the correct SDK is chosen here as well.
  - Provide argument
- For testing:
    - Run the following for MainScene.java:
      -     --image src/teapot.txt --speed 10 --dir 8, 4, 10  
- Click Apply and Run 
- Follow instructions in Section 2 for usage.
