# Group2

## Members
- Timothy Borunov   - timobohr@bu.edu
- Haven Cook        - haven@bu.edu
- Louis Jimenez     - louisjh@bu.edu
- Sergio Rodriguez  - sergioer@bu.edu


## Project Description

We have chosen the predefined project of the "Bouncing Shape". The purpose of this project is to design and implement a 3D graphics application in Java that will display one or more solids in a window that move around, bounce off walls of a virtual 3D space, and deform when doing so in an algorithmic efficient manner.

For this project, the object needs to be definable by the user, including the initial speed and direction and the “bounciness” of the solid when it interacts with other surfaces. The user should also be able to provide a path to an image file in the form of a textual list of triangles’ coordinates to construct their own shape. Frame rate of the bounce simulation is presented in the window every second.

Some additional features we've implemented past the baseline requirements include the option to draw multiple shapes to the screen, a physics engine that makes the movement more true to life with gravity and deformation, multiple obstacles on screen for these shapes to interact with, a game with a scoring system and user interaction with the mouse to control the shape, and light sources with shading.

## Implementation

We utilized the JavaFX library which is an open-source graphics library for Java. This is a relatively lightweight library with ease of use at the cost of performance. The initial reading of data constructs an Adjacency list which permits us to easily read in each vertex and access adjacency for further operations. Using the Adjacency List we base a TriangleMesh off of it to store the vertices of each triangle and respective face which constructs the object. Since we look at each vertex a constant number of times, the preprocessing for the object and javaFX simulation is at most O(n) time.

The simulation is then drawn at most 60 frames a second. For each frame, all code including collisions, obstacles, camera, and physics is updated just once and runs for a small constant time period. 

The way movement is done is by multiplying the original mesh by a translation factor in the x,y,z directions. This can be done efficiently due to the triangle mesh structure and takes only a constant time when printing to the graphics window. Therefore, all of our current physics and collisions are done only in x, y, and z axes, meaning that we do not currently implement rotational velocity and physics.

We now have a UI which allows you to select from multiple objects or upload your own by selecting a file on your computer. Additionally you are able to specify the number of obstacles and game elements you would like to generate as well with up to 3 different bouncing shapes.

Physics is implemented by integration with the MovingObject where the velocity and acceleration are changed for the object itself and update the translation of the original mesh. Same for collisions which directly are handled by accessing MovingObject elements in constant time.

Deformation is limited to bouncing along each side without rotation, simplifying data structure design to be able to cache points of deformation where the object collides on its side and then deforms and reforms by updating the translation of individual points. This runs in O(V) time on initial calculation which is done in preprocessing, but only a fraction of that time when doing actual deformation. The amount of deformation scales with the speed of impact.

We have two separate files that we are currently working with: [GraphicsAndWindowTest](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/org/example/newmat/GraphicsAndWindowsTest.java?ref_type=heads) and [MainScene](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/org/example/newmat/MainScene.java?ref_type=heads).

[MainScene](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/org/example/newmat/MainScene.java?ref_type=heads) is the UI of our project which allows the user to configure the three runable figures from preset options, use commandline arguments, or provide an image from their file system. It also allows for the number of obstacles to be set. There are two types, one type is immovable, and the other type is the game obstacle which disappears on collision. This menu allows you to launch [GraphicsAndWindowTest.java](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/org/example/newmat/GraphicsAndWindowsTest.java?ref_type=heads).

[GraphicsAndWindowTest.java](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/org/example/newmat/GraphicsAndWindowsTest.java?ref_type=heads)  is the game version of our code which has multiple objects able to run with solid obstacles and also removable obstacles which are used in the game. Essentially the shapes can be bounced by clicking and dragging on the xy axis and using W/S keys on the keyboard to move the shape on the z axis. The camera can be turned using the arrow keys. When the shapes hit orange obstacles, they bounce off and if they hit green obstacles, the obstacles are consumed and the score counter in the upper corner goes up. In so doing we demonstrate all the features together in this test as the game. However, for this file, we recommend using our teapot objects which we document in the install section because we are able to run this with a higher frame count. It will be optimized by the final report in two weeks.

[MovingObject](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Shape/MovingObject.java?ref_type=heads)  is a class that holds the Moving object implementation in which we contain the adjacency list, triangle mesh, and current translation of the object along with physics, velocity, and acceleration.

[Obstacles](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Shape/Obstacle.java?ref_type=heads) class represents a 3D object that can be used in a JavaFX scene.This class allows for the creation of obstacles with different shapes and colors, and can handle collision detection against other objects within the scene.

[ObstaclesField](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Shape/ObstacleField.java?ref_type=heads) class manages a collection of Obstacle objects in a 3D space. It allows for the dynamic creation of obstacles, managing their positions, and checking for collisions.

[VisibleWindow](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Shape/VisibleWindow.java?ref_type=heads) class manages the transparent cube which represents the windows for our application.

[readFile](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Graph/readFile.java?ref_type=heads) This class is generated for each MovingObject to manage points and parse files to create an adjacency list.

[VertexKey](https://agile.bu.edu/gitlab/ec504/ec504_projects/group2/-/blob/master/src/main/java/Graph/VertexKey.java?ref_type=heads) Helper class for readFile to be able to add float arrays as a key to a hashmap.


## List of Features
- [ ] Add 100 fixed (immovable) obstacles to the environment of the bouncing figure. [15%]
    - To implement the 100 obstacles, we made two classes. One is the Obstacle class which takes in the x,y, and z coordinates of the  obstacle as well as customizable parameters such as the ability to remove it, the color, the shape, and the size. We also had an ObstacleField class which stores an array of Obstacles and randomizes their position within the window. Within these classes, we utilized the Bounds class which creates a Cube around the shape for collision detection. (100 % done)
- [ ] Add a physics engine to your bouncing shape, including a gravity source and a model for the elasticity of triangle edges. [15%]
    - The physics engine is embedded for the objects where at each frame the object's physics is updated, using the frames as the measure of time for each update for both velocity and acceleration. The velocity and acceleration simply changes the translation of the image mentioned in the implementation. Collision is done the same way where we check the bounds of an object using the triangleMesh structure and change the translation accordingly if the bounds overlap with the bounds of another object. (100 % done)
- [ ] Bounce around more than one (1-million triangle) figures at the same time. [+15% per each additional figure, max of 2]
    - We just implement the object as a separate class which means we can instantiate multiple instances of the same object with different parameters. Currently multiple objects with 1 million triangles is too slow in fps which is why we have separate files we demonstrate. (100 % done)
- [ ] Develop this into a game that includes the mouse to affect the motion of the figure and maintains scoring based on object collisions. [30%] 
    - To implement the game aspect, we used JavaFX internal box coordinates as anchor and release locations which would send the objects flying towards the direction of the mouse release. The calculation of the launch velocity was calculated based on the distance of those two coordinate locations. For 3rd direction movement, we implemented a keyboard input which takes W or S keyboard input to move the figures into that direction. 
        - Created a setVelocity in MovableObject class to facilitate this process
    - We updated the removable element on some of the obstacles so that when the obstacles passed through, it would gain a set score and eliminate the object. Only half of the 100 obstacles are removable and would update the score. (100 % done)


## References

### Resources
[JavaFX Tutorial Playlist](https://www.youtube.com/watch?v=mbK2xqG2glM&list=PLhs1urmduZ295Ryetga7CNOqDymN_rhB_) 
### 3D Models
- [ ] [1 Million Triangle Sphere (Not Modified)](https://sketchfab.com/3d-models/1m-polygon-sphere-f9f8de6b68e5434b86e86c9b5eca086a)
- [ ] [Utah Teapot](https://users.cs.utah.edu/~natevm/newell_teaset/)
- [ ] [Armadillo](https://graphics.stanford.edu/data/3Dscanrep/)

## Work Breakdown

### Timothy
- End to end code to translate formatted file from command line to javaFX object
- Made the class for the bouncing shape and all the included methods and fields
- Made it possible to bounce multiple objects
- Created collisions between the side of the window and obstacles
- Updated deformation to properly change the shape and make it return to original form
- Updated camera to allow easy viewing of the game
- Updated deformation to save old deformations and reuse them to increase frame rate by caching deformation
- Debugged deformation to no longer cause bugs with specific side collisions

### Haven
- Wrote commandline parser for the first object and read in of file
- Implemented lighting and environment
- Wrote auxiliary program to convert .obj files to the proper format
- Added in file path functionality to the main menu
- Co-created UI menu to allow the user to modify aspects of the application

### Louis
- Minimum requirement bouncing figure of a million triangles that bounces at 1 fps
- Wrote code to show FPS on the window
- Created the 100 obstacles and modified them to fit our game
- Created visible normalized window
- Normalized Coordinates
- Co-created UI menu to allow the user to modify aspects of the application

### Sergio
- Java Documentation
- Game Elements 
- Mouse and keyboard user input which impacts the velocity
- Score calculation

Signature: Timothy Borunov, Haven Cook, Louis Jimenez-Hernandez, Sergio Rodriguez
