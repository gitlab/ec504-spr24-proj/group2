## Accepted feedback

Change  0:  Issue #8. We updated the shape to fulfill the basic requirements of starting in the middle of a normalized window.

Change  1:  Issue #9. Shoot line position was forced to be on one window previously, but now we have the line properly update without reliant on camera rotation so that the line is independent of window.

Change  2:  Issue #17. Bouncing with the mouse was previously not responsive. Now with the changed line it is working correctly and direction is more easily observed.

Change  3:  Issue #16. We added a reset button, ESC key to reset to the UI page allowing users to restart and reset the simulation



## Rejected feedback

Change  0:  Issue #12. We reject the lighting changes because we discontinued that feature and decided to keep it at that stage after discussion with instructor.

Change  1:  Issue #15. Floor should block box view and show not be removed as the floor is meant to be an opaque object.

Change  2:  Issue #11. Cool concept to have a splitable object, but we simply do not intend to make that due to the game concept we are utilizing.

Change  3:  Issue #10, Issue #7, Issue #13, Issue #14. We did not have time to implement these changes.

Change  4:  Issue #5. We tried to implement this but ran into issues with creating an object off of the JavaFX and we ran out of time. We document this in the README. 

Change  5:  Issue #6. Warning is for non lab computers and our priority is to focus on lab computer specifications.

## Other Changes

Change 0: We added a user UI to modify shapes and obstacles.
