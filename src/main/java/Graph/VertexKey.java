package Graph;

import java.util.Arrays;

/**
 * Represents a vertex in a graph using its coordinates as the key.
 */
public class VertexKey {

    /**
     * The coordinates of the vertex.
     */
    private final float[] coords;
    private Integer index;

    /**
     * Constructs a new {@code VertexKey} instance with the specified coordinates.
     *
     * @param coordinates The coordinates of the vertex. It's assumed that the array represents a point
     *                    in n-dimensional space, where the dimensionality is determined by the array's length.
     */
    public VertexKey(float[] coordinates) {
        this.coords = coordinates;
    }

    public void setIndex(Integer in) {
        this.index = in;
    }

    public Integer getIndex() {
        return this.index;
    }

    /**
     * Compares this vertex key to another object for equality.
     * <p>
     * Two {@code VertexKey} instances are considered equal if their coordinate arrays are equal.
     *
     * @param anotherVertex The object to be compared for equality with this vertex key.
     * @return {@code true} if the specified object is equal to this vertex key; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object anotherVertex) {
        if (anotherVertex == this) {
            return true;
        }
        if (anotherVertex == null) {
            return false;
        }
        if (anotherVertex.getClass() != this.getClass()) {
            return false;
        }
        VertexKey vertexKey = (VertexKey) anotherVertex;
        return Arrays.equals(this.coords, vertexKey.coords);
    }

    /**
     * Returns a hash code value for this vertex key.
     *
     * @return A hash code value for this vertex key.
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.coords);
    }

    /**
     * Returns the coordinates of this vertex.
     *
     * @return An array of floats representing the coordinates of this vertex.
     */
    public float[] getPoint() {return this.coords;}
}