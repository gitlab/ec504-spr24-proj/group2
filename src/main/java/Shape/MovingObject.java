package Shape;

import Graph.readFile;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Translate;

import java.util.HashSet;

/**
 * Represents a moving object in a 3D space with deformable capabilities.
 * The object's movement and deformation are managed by physics properties and vertex manipulations.
 */
public class MovingObject {
    private readFile fileObject;
    private final float[] texCoords;
    private final TriangleMesh mesh;
    private MeshView triangleView;
    private final Translate translate;
    private double[] velocityVec, directionVec;
    private double speed;
    private final Deformation[] deformDir;


    /**
     * Default constructor which initializes the object with default properties.
     */
    MovingObject() {
        texCoords = new float[2];
        mesh = new TriangleMesh();
        translate = new Translate();
        velocityVec = new double[3];
        directionVec = new double[3];
        deformDir = new Deformation[6];
    }

    /**
     * Constructs a MovingObject with parameters for initializing readFile and other physics and mesh properties.
     * @param parameters Array of strings used for initialization parameters.
     */
    public MovingObject(String[] parameters) {
        this();
        fileObject = new readFile(parameters);
        fileObject.makeVertexHash(fileObject.getPath());
        directionVec = fileObject.getDirection();
        speed = fileObject.getSpeed();
        initVelocity();
        velocityVec = getVelocityVec();
        mesh.getPoints().setAll(fileObject.getVertices());
        mesh.getFaces().setAll(fileObject.getFaces());
        triangleView = new MeshView(mesh);
      //  doKMeans();
        setTempDefault();
        populateDeformations();
        //setCentroid();
    }

    /**
     * Sets the texture coordinates for the object's mesh.
     * @param t1 Texture coordinate along the X-axis.
     * @param t2 Texture coordinate along the Y-axis.
     */
    public void setTexCoords(float t1, float t2) {
        texCoords[0] = t1;
        texCoords[1] = t2;
        mesh.getTexCoords().setAll(texCoords);
        triangleView.setMesh(mesh);
    }
    
    /**
     * Sets the translation of the object in 3D space.
     * @param x Translation along the X-axis.
     * @param y Translation along the Y-axis.
     * @param z Translation along the Z-axis.
     */
    public void setTranslate(double x, double y, double z) {
        translate.setX(x);
        translate.setY(y);
        translate.setZ(z);
        triangleView.getTransforms().add(translate);
    }

    /**
     * Updates the translation of the object in 3D space based on provided offsets.
     * @param x Offset along the X-axis.
     * @param y Offset along the Y-axis.
     * @param z Offset along the Z-axis.
     */
    public void updateTranslate(double x, double y, double z) {
        //velocityVec[1] += gravity;
        translate.setX(translate.getX() + x);
        translate.setY(translate.getY() + y);
        translate.setZ(translate.getZ() + z);
    }

    /**
     * Retrieves the velocity vector of the object.
     * @return Array of doubles representing the velocity in each spatial dimension.
     */
    public double[] getVelocityVec() {
        return velocityVec;
    }

    /**
     * Sets the velocity vector of the object.
     * @param x Velocity along the X-axis.
     * @param y Velocity along the Y-axis.
     * @param z Velocity along the Z-axis.
     */
    public void setVelocityVec(double x,double y, double z) {
        velocityVec[0] = x;
        velocityVec[1] = y;
        velocityVec[2] = z;
    }


    /**
     * Initializes the velocity vector based on the object's direction and speed.
     */
    public void initVelocity() {
        for (int ii = 0; ii < 3; ii++) {
            velocityVec[ii] = directionVec[ii] * speed;
        }
    }


    /**
     * Sets default rendering settings for the object's mesh view.
     */
    public void setTempDefault() {
        triangleView.setCullFace(CullFace.NONE);
        triangleView.setMaterial(new PhongMaterial(Color.BLUE));
        setTexCoords(0,0);
    }

    /**
     * Returns the mesh view associated with the object.
     * @return MeshView of the object.
     */
    public MeshView getMesh() {
        return triangleView;
    }


    /**
     * Retrieves the updated centroid of the object considering its translation.
     */
    public void populateDeformations() {
        for (int axis = 0; axis < 3; axis++) {
            for (int side = 0; side < 2; side++) {
                addDeformation(side, axis);
            }
        }
    }
    /**
     * Applies a deformation to the object based on the specified side and axis.
     * @param side Side of the deformation (either positive or negative direction).
     * @param axis Axis of deformation (0 for X-axis, 1 for Y-axis, 2 for Z-axis).
     //* @param speed Magnitude of deformation.
     */

    public void deformShape(int side, int axis, double speed) {
        float[] points = mesh.getPoints().toArray(null);
        if (deformDir[axis*2 + side].deformed) {
            return;
        }
        System.out.println("Deformed = " + side + "\n Speed = " + speed);
        int decrement = 10;
        int counter = 0;
        double newSpeed = Math.abs(speed);
        newSpeed -= decrement;
        if (newSpeed > 0) {
            deformDir[axis*2+side].deformed = true;
            counter++;
            for (Integer k : deformDir[axis*2 + side].layer5) {
                if (side == 0) {
                    points[k] += (float) deformDir[axis * 2 + side].deformation;
                } else {
                    points[k] -= (float) deformDir[axis * 2 + side].deformation;
                }
            }
        }
        newSpeed -= decrement;
        if (newSpeed > 0) {
            counter++;
            for (Integer k : deformDir[axis*2 + side].layer4) {
                if (side == 0) {
                    points[k] += (float) deformDir[axis * 2 + side].deformation;
                } else {
                    points[k] -= (float) deformDir[axis * 2 + side].deformation;
                }
            }
        }
        newSpeed -= decrement;
        if (newSpeed > 0) {
            counter++;
            for (Integer k : deformDir[axis*2 + side].layer3) {
                if (side == 0) {
                    points[k] += (float) deformDir[axis * 2 + side].deformation;
                } else {
                    points[k] -= (float) deformDir[axis * 2 + side].deformation;
                }
            }
        }
        newSpeed -= decrement;
        if (newSpeed > 0) {
            counter++;
            for (Integer k : deformDir[axis*2 + side].layer2) {
                if (side == 0) {
                    points[k] += (float) deformDir[axis * 2 + side].deformation;
                } else {
                    points[k] -= (float) deformDir[axis * 2 + side].deformation;
                }
            }
        }
        newSpeed -= decrement;
        if (newSpeed > 0) {
            counter++;
            for (Integer k : deformDir[axis*2 + side].layer1) {
                if (side == 0) {
                    points[k] += (float) deformDir[axis * 2 + side].deformation;
                } else {
                    points[k] -= (float) deformDir[axis * 2 + side].deformation;
                }
            }
        }
        deformDir[axis*2+side].counter = counter;
        System.out.println("Deformed = " + deformDir[axis*2+side].counter);
        mesh.getPoints().setAll(points);
    }

    public void addDeformation(int side, int axis) {

        float[] points = mesh.getPoints().toArray(null);
        double half;
        double trans;
        double side2;
        Bounds meshBounds = triangleView.getBoundsInParent();
        if (axis == 0) {
            trans = translate.getX();
            half = Math.abs(meshBounds.getMaxX() - meshBounds.getMinX())/2.0;
            if (side == 0) {
                side2 = meshBounds.getMinX();
            }   else {
                side2 = meshBounds.getMaxX();
            }
        }   else if (axis == 1) {
            trans = translate.getY();
            half = Math.abs(meshBounds.getMaxY() - meshBounds.getMinY())/2.0;
            if (side == 0) {
                side2 = meshBounds.getMinY();
            }   else {
                side2 = meshBounds.getMaxY();
            }
        }   else {
            trans = translate.getZ();
            half = Math.abs(meshBounds.getMaxZ() - meshBounds.getMinZ())/2.0;
            if (side == 0) {
                side2 = meshBounds.getMinZ();
            }   else {
                side2 = meshBounds.getMaxZ();
            }
        }
        Deformation newDeform = new Deformation(half / 5.0, side);

        for (int j = axis; j < points.length; j += 3) {
            if (Math.abs(points[j] + trans - side2) < half) {
                newDeform.layer1.add(j);
                //points[j] += newDeform.deformation;
                if (Math.abs(points[j] + trans - side2) < (4 * half / 5.0)) {
                    newDeform.layer2.add(j);
                    //points[j] += newDeform.deformation;
                    if (Math.abs(points[j] + trans - side2) < (3 * half/5.0)) {
                        newDeform.layer3.add(j);
                        //points[j] += newDeform.deformation;
                        if (Math.abs(points[j] + trans - side2) < (2 * half/5.0)) {
                            newDeform.layer4.add(j);
                            //points[j] += newDeform.deformation;
                            if (Math.abs(points[j] + trans - side2) < (half/5.0)) {
                                newDeform.layer5.add(j);
                                //points[j] += newDeform.deformation;
                            }
                        }

                    }
                }
            }
        }
        //mesh.getPoints().setAll(points);
        newDeform.deformed = false;
        deformDir[axis*2+side] = newDeform;
    }

    /**
     * Reverses any deformations applied to the object.
     */
    public void unDeformShape() {
        float[] points = mesh.getPoints().toArray(null);
        for (Deformation d : deformDir) {
            if (d != null) {
                if (d.deformed) {
                    System.out.println("Undeformed = " + d.counter);
                    if(d.counter == 5) {
                        for (Integer k : d.layer1) {
                            if (d.side == 0) {
                                points[k] -= (float) d.deformation;
                            } else {
                                points[k] += (float) d.deformation;
                            }
                        }
                        d.counter--;
                    } else if (d.counter == 4) {
                        for (Integer k : d.layer2) {
                            if (d.side == 0) {
                                points[k] -= (float) d.deformation;
                            } else {
                                points[k] += (float) d.deformation;
                            }
                        }
                        d.counter--;
                    } else if (d.counter == 3) {
                        for (Integer k : d.layer3) {
                            if (d.side == 0) {
                                points[k] -= (float) d.deformation;
                            } else {
                                points[k] += (float) d.deformation;
                            }
                        }
                        d.counter--;
                    } else if (d.counter == 2) {
                        for (Integer k : d.layer4) {
                            if (d.side == 0) {
                                points[k] -= (float) d.deformation;
                            } else {
                                points[k] += (float) d.deformation;
                            }
                        }
                        d.counter--;
                    } else if (d.counter == 1) {
                        for (Integer k : d.layer5) {
                            if (d.side == 0) {
                                points[k] -= (float) d.deformation;
                            } else {
                                points[k] += (float) d.deformation;
                            }
                        }
                        d.counter--;
                        d.deformed = false;
                    } else {
                        System.out.println("BUG in Undeform");
                    }

                    mesh.getPoints().setAll(points);
                }
            }
        }

    }

    private static class Deformation {

        /**
         * Constructs a deformation with a specified magnitude.
         * @param deform Magnitude of deformation.
         */
        Deformation(double deform, int s) {
            deformed = false;
            layer1 = new HashSet<>();
            layer2 = new HashSet<>();
            layer3 = new HashSet<>();
            layer4 = new HashSet<>();
            layer5 = new HashSet<>();
            counter = 0;
            deformation = deform;
            side = s;
        }
        public int side;

        public boolean deformed;
        public int counter;
        public double deformation;

        public HashSet<Integer> layer1;
        public HashSet<Integer> layer2;
        public HashSet<Integer> layer3;
        public HashSet<Integer> layer4;
        public HashSet<Integer> layer5;

    }
}
