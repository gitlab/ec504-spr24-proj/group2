package Shape;

import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.*;

/**
 * The Obstacle class represents a 3D object that can be used in a JavaFX scene.
 * This class allows for the creation of obstacles with different shapes and colors,
 * and can handle collision detection against other objects within the scene.
 */
public class Obstacle {

    public int obSize = 10;
    public int WindowSize = 0;
    public double xTranslation;
    public double yTranslation;
    public double zTranslation;
    public Node shape;
    public Bounds shapeBound;
    public boolean removable;

    /**
     * Constructs an Obstacle object with specified properties.
     *
     * @param WindowSize    The size of the window in which the obstacle exists.
     * @param xTranslation  The x translation of the obstacle from the origin.
     * @param yTranslation  The y translation of the obstacle from the origin.
     * @param zTranslation  The z translation of the obstacle from the origin.
     * @param obSize        The size of the obstacle.
     * @param shape         The geometric shape of the obstacle (Box, Sphere, or Cylinder).
     * @param color         The color of the obstacle.
     * @param removable     Specifies whether the obstacle is removable or not.
     */
    public Obstacle(int WindowSize, double xTranslation, double yTranslation,
                    double zTranslation, int obSize, String shape, Color color
            , boolean removable){
        this.obSize = obSize/2;
        this.WindowSize = WindowSize;
        this.xTranslation = xTranslation + (double) obSize/2;
        this.yTranslation = yTranslation + (double) obSize/2;
        this.zTranslation = zTranslation + (double) obSize/2;
        this.shape = makeNode(shape, color);
        this.shape.setTranslateX(xTranslation);
        this.shape.setTranslateY(yTranslation);
        this.shape.setTranslateZ(zTranslation);
        this.removable = removable;
        shapeBound = this.shape.getBoundsInParent();
    }

    /**
     * Creates a node based on the specified shape and color.
     *
     * @param shape The shape type ("Box", "Sphere", or "Cylinder").
     * @param color The color of the shape.
     * @return Node representing the shape with the specified properties.
     */
    public Node makeNode(String shape, Color color){
        if(shape.equals("Box")){
            Box b = new Box(obSize,obSize,obSize);
            b.setMaterial(new PhongMaterial(color));
            return b;
        } else if (shape.equals("Sphere")){
            Sphere s = new Sphere(obSize);
            s.setMaterial(new PhongMaterial(color));
            return s;
        } else {
            Cylinder c = new Cylinder(obSize,obSize);
            c.setMaterial(new PhongMaterial(color));
            return c;
        }
    }

    /**
     * Returns the shape of the obstacle as a Node.
     *
     * @return Node representing the obstacle's shape.
     */
    public Node getShape(){
        return shape;
    }

    /**
     * Adds the shape of this obstacle to a specified group.
     *
     * @param group The group to which the shape will be added.
     */
    public void addToGroup(Group group){
        group.getChildren().add(shape);
    }

    /**
     * Checks for collision between this obstacle and another bounding box.
     *
     * @param otherBounds The bounds of the other object to check for collision.
     * @return A string indicating the side of collision ("LEFT", "RIGHT", "TOP", "BOTTOM", "FRONT", "BACK", "REMOVABLE", "NONE").
     */
    public String checkCollision(Bounds otherBounds) {
        if (shapeBound.intersects(otherBounds)) {
            if(removable){
                return "REMOVABLE";
            }
            //Figures out distance between bounds for collision detection
            double deltaX = Math.min(shapeBound.getMaxX() - otherBounds.getMinX(), otherBounds.getMaxX() - shapeBound.getMinX());
            double deltaY = Math.min(shapeBound.getMaxY() - otherBounds.getMinY(), otherBounds.getMaxY() - shapeBound.getMinY());
            double deltaZ = Math.min(shapeBound.getMaxZ() - otherBounds.getMinZ(), otherBounds.getMaxZ() - shapeBound.getMinZ());

            // Determine minimum overlap
            if (deltaX < deltaY && deltaX < deltaZ) {
                if (shapeBound.getMaxX() > otherBounds.getMaxX()) {
                    return "RIGHT";
                } else {
                    return "LEFT";
                }
            } else if (deltaY < deltaZ) {
                if (shapeBound.getMaxY() > otherBounds.getMaxY()) {
                    return "TOP";
                } else {
                    return "BOTTOM";
                }
            } else {
                if (shapeBound.getMaxZ() > otherBounds.getMaxZ()) {
                    return "FRONT";
                } else {
                    return "BACK";
                }
            }
        }
        return "NONE";
    }

}
