package Shape;

import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;

import java.util.*;

/**
 * The ObstacleField class manages a collection of Obstacle objects in a 3D space.
 * It allows for the dynamic creation of obstacles, managing their positions, and checking for collisions.
 */
public class ObstacleField {

    public int windowSize, obSize;
    public int xTranslation, yTranslation, zTranslation;
    Set<String> occupiedPositions = new HashSet<>();
    ArrayList<Obstacle> obstacles = new ArrayList<>();
    ArrayList<Bounds> obstacleBounds = new ArrayList<>();
    /**
     * Constructs an ObstacleField with a specified number of obstacles.
     *
     * @param windowSize    The size of the window in which the obstacles are placed.
     * @param xTranslation  The x offset for the initial translation of obstacles.
     * @param yTranslation  The y offset for the initial translation of obstacles.
     * @param range         The range within which random positions will be generated.
     * @param min           The minimum value for the position offsets.
     * @param obSize        The size of each obstacle.
     * @param objNum        The number of obstacles to generate.
     * @param shape         The shape of the obstacles (e.g., "Box", "Sphere").
     * @param color         The color of the obstacles.
     * @param Removable     Indicates whether the obstacles are immovable.
     */
    public ObstacleField(int windowSize, int xTranslation, int yTranslation,
                         int range, int min, int obSize, int objNum,
                         String shape, Color color, boolean Removable){
        this.obSize = obSize/2;
        this.windowSize = windowSize;
        this.xTranslation = xTranslation + obSize/2;
        this.yTranslation = yTranslation + obSize/2;
        zTranslation = obSize/2;

        double x, y, z;
        String posKey;
        for(int i = 0; i<objNum; i++){
            Random rand = new Random();
            do {
                x = rand.nextDouble() * range + min; // Random position for x
                y = rand.nextDouble() * range + min; // Random position for y
                z = rand.nextDouble() * range + min; // Random position for z
                posKey = x + "," + y + "," + z;   // Create a key for the set
            } while (occupiedPositions.contains(posKey)); // Ensure uniqueness

            occupiedPositions.add(posKey); // Add the new position to the set

            Obstacle ob = new Obstacle(this.windowSize, this.xTranslation + x, this.yTranslation +y,
                    this.zTranslation + z, this.obSize, shape, color, Removable);
            obstacles.add(ob);
            obstacleBounds.add(ob.getShape().getBoundsInParent());
        }
    }

    /**
     * Adds all obstacle shapes to a specified group.
     *
     * @param group The group to which the obstacle shapes are added.
     */
    public void addToGroup(Group group){
        for(Obstacle o: obstacles){
            group.getChildren().add(o.getShape());
        }
    }

    /**
     * Returns the list of obstacles.
     *
     * @return ArrayList of Obstacle objects.
     */
    public ArrayList<Obstacle> getObstacles() {
        return obstacles;
    }

    /**
     * Returns the bounds of all obstacles in this field.
     *
     * @return ArrayList of Bounds objects.
     */
    public ArrayList<Bounds> getObstacleBounds() {
        return obstacleBounds;
    }

    /**
     * Checks for collision between the bounds of any obstacle in the field and another set of bounds.
     *
     * @param otherBounds The bounds of another object to check for collisions.
     * @return List containing collision information and the obstacle involved, or "NONE" and null if no collision.
     */
    public List<Object> checkCollision(Bounds otherBounds) {
        for (Obstacle o : obstacles) {
            if(o.shapeBound.intersects(otherBounds)){
                return Arrays.asList(o.checkCollision(otherBounds), o);
            }
        }
        return Arrays.asList("NONE", null); // No collision detected
    }

    /**
     * Adds an obstacle to the field.
     *
     * @param o The obstacle to be added.
     */
    public void addToField(Obstacle o){
        obstacles.add(o);
        obstacleBounds.add(o.getShape().getBoundsInParent());
    }

    /**
     * Removes an obstacle from the field.
     *
     * @param o The obstacle to be removed.
     */
    public void removeField(Obstacle o){
        obstacles.remove(o);
    }

}
