package Shape;


import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.scene.transform.Translate;

/**
 * Represents a visible 3D window using JavaFX. This class allows for the creation
 * of a cubic window defined by its size and position in a 3D space.
 */
public class VisibleWindow {


    /**
     * Default constructor initializing a VisibleWindow with predefined size and offsets.
     */
    public VisibleWindow(){
        WindowSize = 300;
        halfWSize = WindowSize/2;
        xOffset = 600;
        xTranslation = halfWSize + xOffset;
        yOffset = 300;
        yTranslation = halfWSize + yOffset;
        zTranslation = halfWSize;
    }

    /**
     * Constructs a VisibleWindow with specific size and offset values.
     * @param WindowSize The size of each side of the cubic window.
     * @param xOffset The x-axis offset for the window's position.
     * @param yOffset The y-axis offset for the window's position.
     */
    public VisibleWindow(int WindowSize,  int xOffset, int yOffset){
        this.WindowSize = WindowSize;
        halfWSize = WindowSize/2;
        this.xOffset = xOffset;
        xTranslation = halfWSize + xOffset;
        this.yOffset = yOffset;
        yTranslation = halfWSize + yOffset;
        zTranslation = halfWSize;
    }


    /**
     * Creates and returns a 3D window as a Box object.
     * @return A Box object representing the 3D window with specified dimensions and translations.
     */
    public Box makeWindow(){
        Box window = new Box(WindowSize,WindowSize,WindowSize);
        window.setDrawMode(DrawMode.LINE);
        Translate windowTranslation = new Translate();
        windowTranslation.setX(xTranslation);
        windowTranslation.setY(yTranslation);
        windowTranslation.setZ(zTranslation);
        window.getTransforms().add(windowTranslation);

        return window;
    }

    /**
     * Retrieves the x-axis offset.
     * @return The x-axis offset.
     */
    public int getXOffset() {
        return xOffset;
    }


    /**
     * Retrieves the y-axis offset.
     * @return The y-axis offset.
     */
    public int getYOffset() {
        return yOffset;
    }

    /**
     * Retrieves the size of the window.
     * @return The size of the window on each side.
     */
    public int getWindowSize(){return WindowSize;
    }
    int WindowSize = 0;
    int halfWSize = 0;
    int xOffset, yOffset, xTranslation, yTranslation, zTranslation;
}
