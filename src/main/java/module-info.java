module org.example.newmat {
    requires javafx.controls;
    requires javafx.fxml;
    requires commons.math3;


    opens org.example.newmat to javafx.fxml;
    exports org.example.newmat;
}