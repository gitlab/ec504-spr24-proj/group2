package org.example.newmat;

import Shape.MovingObject;
import Shape.Obstacle;
import Shape.ObstacleField;
import Shape.VisibleWindow;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.geometry.Point3D;
import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.min;


/**
 * Demonstrates advanced JavaFX graphics and window management by creating a 3D scene with
 * animated elements and FPS (Frames Per Second) counter.
 * <p>
 * This application creates a 3D scene containing a textured cube and a dynamically generated
 * mesh based on input parameters. It showcases the handling of 3D transformations, animation,
 * camera movement, and basic input from the keyboard to manipulate the scene. Additionally,
 * it includes a real-time FPS counter overlaid on the scene to monitor performance.
 */
public class GraphicsAndWindowsTest extends Application {
    /**
     * Initializes location for anchor or release locations of mouse for drawing slingLine and calculating
     * the release velocity of the figures.
     */
    public double anchorX, anchorY, releaseX, releaseY;
    /**
     * Initializes the line drawn in the application for mouse control.
     */
    private Line slingLine;
    /**
     * Initializes the MovingObject list used to manipulate and store the different objects created throughout
     * the application.
     */
    public static MovingObject[] objects;

    private Stage stage;
    private final String[] arguments;
    private final String figure1, figure2, figure3;
    private final int normalObstacles, gameObstacles;

    public GraphicsAndWindowsTest(String[] arguments, String figure1, String figure2, String figure3,
                                  int normalObstacles, int gameObstacles){

        this.arguments = arguments;
        this.figure1 = figure1;
        this.figure2 = figure2;
        this.figure3 = figure3;
        this.normalObstacles = normalObstacles;
        this.gameObstacles = gameObstacles;
    }
    /**
     * Starts the JavaFX application.
     * This method initializes the 3D scene with its objects, animation timer, and event handlers.
     *
     * @param stage The primary stage for this application, onto which the scene is set.
     * @throws IOException If there is an issue reading the input file for generating the mesh.
     */
    @Override
    public void start(Stage stage) throws IOException {
        double windowXOffset = 600;
        double windowYOffset = 200;
        VisibleWindow sceneWindow = new VisibleWindow(500, -250, -250);
        Box ourWindow = sceneWindow.makeWindow();


        //Do to sceneWindow.getX and YOffsets() the range assume you start from the origin of the visible window
        ObstacleField obs = new ObstacleField(sceneWindow.getWindowSize(),sceneWindow.getXOffset(),
                sceneWindow.getYOffset(),450,0, 50, normalObstacles, "Box",
                Color.ORANGERED, false);

        ObstacleField obsRemove = new ObstacleField(sceneWindow.getWindowSize(),sceneWindow.getXOffset(),
                sceneWindow.getYOffset(),450,0, 50, gameObstacles, "Box",
                Color.GREEN, true);

        for(Obstacle o: obsRemove.getObstacles()){
            obs.addToField(o);
        }
        String[] ob1 = {"--image", figure1, "--speed", "100", "--dir", "3,", "5,", "7"};
        String[] ob2 = {"--image", figure2, "--speed", "32", "--dir", "4,", "7,", "10"};
        String[] ob3 = {"--image", figure3, "--speed", "5", "--dir", "1,", "1,", "1"};
        //String[] ob4 = {"--image", "src/teapot.txt", "--speed", "100", "--dir", "3,", "5,", "7"};
        //MovingObject newObject1 = new MovingObject(arguments);
        MovingObject newObject1;
        if(figure1.equals("Use CL")){
            newObject1 = new MovingObject(arguments);
        } else{
            newObject1 = new MovingObject(ob1);
        }
        MovingObject newObject2;
        if(figure2.equals("Use CL")){
            newObject2 = new MovingObject(arguments);
        } else{
            newObject2 = new MovingObject(ob2);
        }
        MovingObject newObject3;
        if(figure3.equals("Use CL")){
            newObject3 = new MovingObject(arguments);
        } else{
            newObject3 = new MovingObject(ob3);
        }
//        MovingObject newObject2 = new MovingObject(ob2);
//        MovingObject newObject3 = new MovingObject(ob3);
        //MovingObject newObject4 = new MovingObject(ob4);
        objects = new MovingObject[3];
        objects[0] = newObject1;
        objects[1] = newObject2;
        objects[2] = newObject3;
        //objects[3] = newObject4;


        // Create a grassy looking floor to contextualize the environment
        Box floor = new Box(100000, 10, 100000);
        floor.setTranslateY(255);
        floor.setTranslateZ(250);
        floor.setMaterial(new PhongMaterial(Color.LIGHTGREEN));

        Group group3D = new Group(); // Group of our 3D elements
        group3D.getChildren().add(floor);
        obs.addToGroup(group3D);
        for (MovingObject o : objects ) {
            Bounds boundsO = o.getMesh().getBoundsInParent();
            double centerX = ((double) sceneWindow.getWindowSize() /2) - (boundsO.getMinX() + boundsO.getMaxX()) / 2;
            double centerY = ((double) sceneWindow.getWindowSize() /2) - (boundsO.getMinY() + boundsO.getMaxY()) / 2;
            double centerZ = ((double) sceneWindow.getWindowSize() /2) - (boundsO.getMinZ() + boundsO.getMaxZ()) / 2 ;
            o.setTranslate(sceneWindow.getXOffset() + centerX, sceneWindow.getYOffset() + centerY,centerZ);
            group3D.getChildren().add(o.getMesh());
        }

        // Create a light source to illuminate the scene beyond flat ambient lighting
        PointLight lightSource = new PointLight(Color.GRAY);
        lightSource.setTranslateY(-300);
        lightSource.setTranslateZ(250);

        group3D.setTranslateX(windowXOffset);
        group3D.setTranslateY(windowYOffset);
        group3D.setTranslateZ(250);
        group3D.getChildren().add(ourWindow);
        group3D.getChildren().add(new AmbientLight(Color.GRAY));
        group3D.getChildren().add(lightSource);

        slingLine = new Line(0,0,0,0);


        // NOTE: Due to the perspectiveCamera it looks like it is rotating even though it isn't
        // If you comment out this line and the scene.setCamera(camera) line
        // You will see it move without the rotation effect
        // I think this is pretty cool
        Camera camera = new PerspectiveCamera();
        camera.setNearClip(.1);
        camera.setFarClip(2000);
        camera.setTranslateX(-125);
        camera.setTranslateY(-250);
        camera.setTranslateZ(-250);

        //https://stackoverflow.com/questions/65510979/move-camera-relative-to-view-in-javafx
        //Incorporated the code from this post
        Point3D xAxis = new Point3D(1,0,0);
        Point3D yAxis = new Point3D(0,1,0);
        Rotate groupRotX = new Rotate(0, xAxis);
        Rotate groupRotY = new Rotate(0, yAxis);


        group3D.getTransforms().addAll(groupRotY, groupRotX);

        stage.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            switch(event.getCode()){
                case LEFT:
                    groupRotY.setAngle(groupRotY.getAngle() + 10);
                    break;
                case RIGHT:
                    groupRotY.setAngle(groupRotY.getAngle() - 10);
                    break;
                case UP:
                    groupRotX.setAngle(groupRotX.getAngle() + 10);
                    break;
                case DOWN:
                    groupRotX.setAngle(groupRotX.getAngle() - 10);
                    break;

            }
        });

        /*
         * SubScene to handle 3D components
         * Makes it easier to have 2D and 3D components in the same animation
         */
        SubScene subScene3D = new SubScene(group3D, 1500, 800, true, SceneAntialiasing.BALANCED);
        subScene3D.setFill(Color.LIGHTBLUE);
        subScene3D.setCamera(camera);



        /*
         * Text label to display FPS
         */
        Label fpsLabel = new Label("FPS: 0");
        fpsLabel.setTextFill(Color.WHITE);

        /*
         * Text label to display Score
         */
        Label scoreLabel = new Label("Score: 0");
        fpsLabel.setTextFill(Color.WHITE);
        //Lays out the children in Z order with the last member being front most
        //Effectively allows you to layer multiple elements
        //We are layering our 3D scene with a 2D label on top of it
        // Use Pane instead of StackPane
        Pane layers = new Pane();  // This replaces StackPane for direct control
        layers.setPrefSize(1500, 800); // Set the preferred size to match the scene
        layers.getChildren().add(subScene3D);  // Add the 3D sub scene
        layers.getChildren().addAll(fpsLabel, scoreLabel);  // Add labels

        fpsLabel.setLayoutX(1350); // Position the FPS label in the bottom-right corner
        fpsLabel.setLayoutY(780);
        scoreLabel.setLayoutX(20);  // Position the score label in the top-left corner
        scoreLabel.setLayoutY(20);


        Scene scene = new Scene(layers, 1500, 800);


        /*
        Mouse event for setting anchors
         */
        layers.setOnMousePressed(mouseEvent -> {
            anchorX = mouseEvent.getX();
            anchorY = mouseEvent.getY();
            //System.out.println("Mouse pressed at: " + anchorX + ", " + anchorY);

            slingLine.setStroke(Color.RED);
            slingLine.setStrokeWidth(2);
            slingLine.setStartX(anchorX);
            slingLine.setStartY(anchorY);
            slingLine.setEndX(anchorX);
            slingLine.setEndY(anchorY);
            layers.getChildren().add(slingLine);

        });

        /*
        Mouse event for drawing line as the mouse is dragged through the window.
         */

        layers.setOnMouseDragged(mouseEvent -> {
            double dragX = mouseEvent.getX();
            double dragY = mouseEvent.getY();
            //System.out.println("Mouse dragged to: " + dragX + ", " + dragY);

            // Update the end of the sling line to the current drag position
            slingLine.setEndX(dragX);
            slingLine.setEndY(dragY);
        });

        /*
        Mouse event to launch object and delete the slingLine drawn on window.
         */

        layers.setOnMouseReleased(mouseEvent -> {
            releaseX = mouseEvent.getX();
            releaseY = mouseEvent.getY();
            //System.out.println("Mouse released at: " + releaseX + ", " + releaseY);

            for (MovingObject o : objects) {
                double x = releaseX - anchorX;
                double y = releaseY - anchorY;
                double z = o.getVelocityVec()[2];  // Assume Z is initially unaffected
                x = min(x, 50);
                y = min(y, 50);

                // Calculate new coordinates based on the rotation of the camera
                double cosX = Math.cos(Math.toRadians(abs(groupRotX.getAngle())));
                double sinX = Math.sin(Math.toRadians(abs(groupRotX.getAngle())));
                double cosY = Math.cos(Math.toRadians(abs(groupRotY.getAngle())));
                double sinY = Math.sin(Math.toRadians(abs(groupRotY.getAngle())));

                // Apply transformations
                double newX = x * cosY + z * sinY;
                double newZ = z * cosY - x * sinY;
                double newY = y * cosX + newZ * sinX;
                newZ = newZ * cosX - y * sinX;

                // Set the new velocity vector
                o.setVelocityVec(newX, newY, newZ);
            }

            // Remove the sling visualization line
            layers.getChildren().remove(slingLine);

        });

        /*
        Key Event to move objects in the z dimension by pressing W and S.
         */

        stage.addEventHandler(KeyEvent.KEY_PRESSED, e->{
            if( e.getCode() == KeyCode.W){
                //System.out.println("SPACE");
                for(MovingObject o: objects){
                    double Z = o.getVelocityVec()[2]+5;
                    o.setVelocityVec(o.getVelocityVec()[0], o.getVelocityVec()[1], Z);
                }
            }
            if( e.getCode() == KeyCode.S){
                //System.out.println("SPACE");
                for(MovingObject o: objects){
                    double Z = o.getVelocityVec()[2]-5;
                    o.setVelocityVec(o.getVelocityVec()[0], o.getVelocityVec()[1], Z);
                }
            }

        });



        stage.setTitle("FPS test");
        stage.setScene(scene);
        stage.show();

        // Animation logic to move the Box back and forth
        AnimationTimer timer = new AnimationTimer() {
            private long lastUpdate = 0; //Movement timer
            private long fpsTimer = 0; //Frame timer
            private long frameRate = 0; //Frames per second
            private long score = 0; //Frames per second



            /**
             * Called at every frame while the {@link AnimationTimer} is active to update the scene's elements,
             * including object movements, boundary condition checks, mesh deformation on collision, and FPS counter updates.
             * <p>
             * This method extends the scene update logic with collision detection between the mesh and a virtual bounding box,
             * causing mesh deformation upon nearing the box's edges. It calculates new positions for 3D objects based on their
             * current positions and direction vectors, simulates movement within the scene, and reverses the movement direction
             * at the scene's boundaries. Additionally, it modifies the mesh's vertices to "squish" the mesh when it comes close
             * to or collides with the bounding box, creating a more dynamic interaction between the scene's objects.
             * <p>
             * Movement, boundary checking, and mesh deformation logic are applied based on the bounds of the 3D mesh and cube within the scene.
             * The FPS counter is updated once every second, based on the number of frames rendered in that interval, to provide real-time
             * performance feedback.
             *
             * @param now The timestamp of the current frame given in nanoseconds. This value is used to ensure smooth animations and
             *            consistent updates across frames.
             */
            @Override
            public void handle(long now) {
                //Built in class that helps calculate the bounds of the mesh for collision
                Bounds[] meshBounds = new Bounds[objects.length];
                double[][] direction = new double[objects.length][3];
                for (int i = 0; i < objects.length; i++) {
                    meshBounds[i] = objects[i].getMesh().getBoundsInParent();
                    direction[i] = objects[i].getVelocityVec();
                }


                if (now - lastUpdate >= 16_000_000) {
                    for (MovingObject object : objects) {
                        object.unDeformShape();
                    }
                }


                //Number is roughly 1 mill divided by 60
                if (now - lastUpdate >= 16_000_000) { // Roughly 60 frames per second
                    int lastChanged = 1;
                    double winLeftXBound = sceneWindow.getXOffset();
                    double winTopYBound = sceneWindow.getYOffset();
                    double WindowSize = sceneWindow.getWindowSize();
                    // Reverse direction at bounds
                    for (int i = 0; i < objects.length; i++) {
                        double GRAVITY = 1;
                        double AIRRESISTANCE = 0.03;
                        //Physics updates
                        //Gravity
                        direction[i][1] += GRAVITY;

                        //air resistance computation
                        double mag = 0;
                        for (int j = 0; j < 3; j++ ) {
                            mag += Math.pow(direction[i][j],2);
                        }
                        mag = Math.sqrt(mag);
                        double airResistance = mag * AIRRESISTANCE;
                        direction[i][0] = direction[i][0] - (direction[i][0] / mag * airResistance);
                        direction[i][1] = direction[i][1] - (direction[i][1] / mag * airResistance);
                        direction[i][2] = direction[i][2] - (direction[i][2] / mag * airResistance);


                        double x = direction[i][0];
                        double y = direction[i][1];
                        double z = direction[i][2];


                        // the following will be class methods in the future
                        // Window Collision computation for X
                        if (meshBounds[i].getMinX() + direction[i][0] < winLeftXBound || meshBounds[i].getMaxX() + direction[i][0]  > winLeftXBound + WindowSize) {

                            if (meshBounds[i].getMinX() + x < winLeftXBound) {
                                x = direction[i][0] + (winLeftXBound - (meshBounds[i].getMinX() + x));
                                objects[i].deformShape(0,0,direction[i][0]);
                                //direction[i][0] += 1;
                            } else {
                                x = direction[i][0] - (meshBounds[i].getMaxX() + direction[i][0] - (winLeftXBound + WindowSize));
                                objects[i].deformShape(1,0,direction[i][0]);
                            }
                            direction[i][0] *= -1;
                            lastChanged = 1;
                        }

                        // Window Collision computation for Y
                        if (meshBounds[i].getMinY() + direction[i][1] < winTopYBound || meshBounds[i].getMaxY() + direction[i][1] > winTopYBound + WindowSize) {

                            if (meshBounds[i].getMinY() + y < winTopYBound) {
                                y = direction[i][1] + (winTopYBound - (meshBounds[i].getMinY() + y));
                                objects[i].deformShape(0, 1, direction[i][1]);
                                direction[i][1] += 1;
                            } else {
                                y = direction[i][1] - (meshBounds[i].getMaxY() + direction[i][1] - (winTopYBound + WindowSize));
                                if (Math.abs(direction[i][1]) > 10) {
                                    objects[i].deformShape(1, 1, direction[i][1]);
                                }
                            }
                            direction[i][1] *= -1;
                            lastChanged = 2;
                        }

                        // Window Collision computation for Z
                        if (meshBounds[i].getMinZ() + direction[i][2] < 0 || meshBounds[i].getMaxZ() + direction[i][2] > WindowSize) {
                            if (meshBounds[i].getMinZ() + z < 0) {
                                z = direction[i][2] + (0 - (meshBounds[i].getMinZ() + z));
                                objects[i].deformShape(0,2,direction[i][2]);
                                //direction[i][2] += 1;
                            } else {
                                z = direction[i][2] - (meshBounds[i].getMaxZ() + direction[i][2] - (WindowSize));
                                objects[i].deformShape(1,2,direction[i][2]);
                            }

                            direction[i][2] *= -1;
                            lastChanged = 3;
                        }
                        //Essentially a Pair of (String, corresponding obstacle)
                        List<Object> collisionSide = obs.checkCollision(meshBounds[i]);
                        if(!(collisionSide.get(0).equals("NONE"))){
                            Bounds obBounds = ((Obstacle) collisionSide.get(1)).getShape().getBoundsInParent();
                            //Handles Collision cases
                            switch ((String)collisionSide.get(0)){
                                //For game, increments score and "eats" the obstacle on collision
                                case "REMOVABLE":
                                    Obstacle o = ((Obstacle) collisionSide.get(1));
                                    group3D.getChildren().remove(o.getShape());
                                    obs.removeField(o);
                                    score = score+5;
                                    break;

                                case "LEFT":
                                    direction[i][0] = abs(direction[i][0]);
                                    break;

                                case "RIGHT":
                                    direction[i][0] = -1 * abs(direction[i][0]);
                                    break;

                                case "TOP":
                                    //y = direction[i][1] + (winTopYBound - (meshBounds[i].getMinY() + y) Reference from above
                                    //y = direction[i][1] + (obBounds.getMaxY() - (meshBounds[i].getMinY() + y));
//                                    Random rand = new Random();
//                                    double randomValue = -1 + 2 * rand.nextDouble();
//                                    direction[i][1] += 1;
//                                    direction[i][0] += randomValue;

                                    //direction[i][1] = abs(direction[i][1]) + 1; BEST ALTERNATIVE SO FAR

                                    direction[i][1] = abs(direction[i][1]);
                                    break;

                                case "BOTTOM":
                                    direction[i][1] =  -1 * abs(direction[i][1]);
                                    break;

                                case "FRONT":
                                    direction[i][2] =  abs(direction[i][2]);
                                    break;

                                case "BACK":
                                    direction[i][2] =  -1 * abs(direction[i][2]);
                                    break;

                                default: System.out.println();
                            }
                        }
                        objects[i].updateTranslate(x, y, z);
                        //System.out.println(Arrays.toString(objects[i].getCentroid()));
                        //objects[i].updateRays();
                    }
                    lastUpdate = now;
                }
                if (fpsTimer == 0) {
                    fpsTimer = now; // Initialize the FPS timer
                }
                frameRate++; // Increment frame count for each invocation
                //1 mill nanoseconds in a second
                if (now - fpsTimer >= 1_000_000_000) { // Every second, update the FPS display
                    fpsLabel.setText(String.format("FPS: %d", frameRate));
                    frameRate = 0; // Reset frame count
                    fpsTimer = now; // Reset the FPS timer
                    scoreLabel.setText(String.format("Score: %d", score));
                }

            }
        };
        timer.start();
    }

    /**
     * The entry point of the application. This method is called when the application starts.
     *
     * @param args Command-line arguments passed to the application. Expected to contain
     *             parameters for mesh generation and other configurations.
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}