package org.example.newmat;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

/**
 * The main class that represents the application's main scene.
 */
public class MainScene extends Application {

    /**
     * The main entry point for the application.
     *
     * @param stage The primary stage for the application.
     * @throws Exception If an error occurs during application startup.
     */
    @Override
    public void start(Stage stage) throws Exception {
        Scene menuScene = createMenuScene(stage);
        //Event Handler for ESC key back to the main menu
        stage.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ESCAPE)
            {
                try {
                    stage.setScene(menuScene);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
        stage.setScene(menuScene);
        stage.setTitle("Menu");
        stage.show();
    }

    String figure1 = "None";
    String figure2 = "None";
    String figure3 = "None";

    String filepath = "No File Selected";

    int normalObstacles = 0;
    int gameObstacles = 0;

    /**
     * Creates the main menu scene.
     *
     * @param stage The primary stage for the application.
     * @return The main menu scene.
     */
    private Scene createMenuScene(Stage stage) {

        //LABELS FOR FIGURE SELECTION
        Label fig1Label = new Label("Figure 1");
        fig1Label.setPrefWidth(100);
        fig1Label.setAlignment(Pos.CENTER);
        fig1Label.getStyleClass().add("check-button");
        Label fig2Label = new Label("Figure 2");
        fig2Label.setPrefWidth(150);
        fig2Label.setAlignment(Pos.CENTER);
        fig2Label.getStyleClass().add("check-button");
        Label fig3Label = new Label("Figure 3");
        fig3Label.setPrefWidth(100);
        fig3Label.setAlignment(Pos.CENTER);
        fig3Label.getStyleClass().add("check-button");
        HBox figHBox = new HBox(10); // 10 is the spacing between groups
        figHBox.getChildren().addAll(fig1Label, fig2Label, fig3Label);
        figHBox.setAlignment(Pos.CENTER);

        //FOR FIGURE SELECTION
        // First group of radio buttons
        ToggleGroup group1 = new ToggleGroup();
        RadioButton fig1Arma = new RadioButton("Armadillo");
        fig1Arma.getStyleClass().add("check-button");
        fig1Arma.setToggleGroup(group1);
        RadioButton fig1teapot = new RadioButton("Teapot");
        fig1teapot.getStyleClass().add("check-button");
        fig1teapot.setToggleGroup(group1);
        RadioButton fig1sphere = new RadioButton("Sphere");
        fig1sphere.getStyleClass().add("check-button");
        fig1sphere.setToggleGroup(group1);
        RadioButton fig1CL = new RadioButton("Command Line");
        fig1CL.getStyleClass().add("check-button");
        fig1CL.setToggleGroup(group1);
        RadioButton fig1None = new RadioButton("None");
        fig1None.getStyleClass().add("check-button");
        fig1None.setToggleGroup(group1);
        RadioButton fig1FP = new RadioButton("File Path");
        fig1FP.getStyleClass().add("check-button");
        fig1FP.setToggleGroup(group1);
        VBox fig1Box = new VBox(fig1Arma, fig1teapot, fig1sphere, fig1CL, fig1None, fig1FP);

        // Second group of radio buttons
        ToggleGroup group2 = new ToggleGroup();
        RadioButton fig2Arma = new RadioButton("Armadillo");
        fig2Arma.getStyleClass().add("check-button");
        fig2Arma.setToggleGroup(group2);
        RadioButton fig2teapot = new RadioButton("Teapot");
        fig2teapot.getStyleClass().add("check-button");
        fig2teapot.setToggleGroup(group2);
        RadioButton fig2sphere = new RadioButton("Sphere");
        fig2sphere.getStyleClass().add("check-button");
        fig2sphere.setToggleGroup(group2);
        RadioButton fig2CL = new RadioButton("Command Line");
        fig2CL.getStyleClass().add("check-button");
        fig2CL.setToggleGroup(group2);
        RadioButton fig2None = new RadioButton("None");
        fig2None.getStyleClass().add("check-button");
        fig2None.setToggleGroup(group2);
        RadioButton fig2FP = new RadioButton("File Path");
        fig2FP.getStyleClass().add("check-button");
        fig2FP.setToggleGroup(group2);
        VBox fig2Box = new VBox(fig2Arma, fig2teapot, fig2sphere, fig2CL, fig2None, fig2FP);

        // Third group of radio buttons
        ToggleGroup group3 = new ToggleGroup();
        RadioButton fig3Arma = new RadioButton("Armadillo");
        fig3Arma.getStyleClass().add("check-button");
        fig3Arma.setToggleGroup(group3);
        RadioButton fig3teapot = new RadioButton("Teapot");
        fig3teapot.getStyleClass().add("check-button");
        fig3teapot.setToggleGroup(group3);
        RadioButton fig3sphere = new RadioButton("Sphere");
        fig3sphere.getStyleClass().add("check-button");
        fig3sphere.setToggleGroup(group3);
        RadioButton fig3CL = new RadioButton("Command Line");
        fig3CL.getStyleClass().add("check-button");
        fig3CL.setToggleGroup(group3);
        RadioButton fig3None = new RadioButton("None");
        fig3None.getStyleClass().add("check-button");
        fig3None.setToggleGroup(group3);
        RadioButton fig3FP = new RadioButton("File Path");
        fig3FP.getStyleClass().add("check-button");
        fig3FP.setToggleGroup(group3);
        VBox fig3Box = new VBox(fig3Arma, fig3teapot, fig3sphere, fig3CL, fig3None, fig3FP);

        // Adding listeners to each group
        group1.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                figure1 = chooseFigureStr((RadioButton) newValue);
            }
        });
        group2.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                figure2 = chooseFigureStr((RadioButton) newValue);
            }
        });
        group3.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                figure3 = chooseFigureStr((RadioButton) newValue);
            }
        });

        // Horizontal Layout for Figure selection
        HBox selector = new HBox(10); // 10 is the spacing between groups
        selector.getChildren().addAll(fig1Box, fig2Box, fig3Box);
        selector.setAlignment(Pos.CENTER);

        Label selectedFileLabel = new Label(filepath);
        selectedFileLabel.getStyleClass().add("check-button");

        Button FPButton = new Button("Select File");
        FPButton.getStyleClass().add("menu-button");
        FPButton.setOnAction(event -> {FileChooser fileChooser = new FileChooser();
            File selectedFile = fileChooser.showOpenDialog(stage);
            if (selectedFile != null) {
                filepath = selectedFile.getAbsolutePath();
            }
            selectedFileLabel.setText(filepath);
            if (figure1.equals("No File Selected")) {figure1 = filepath;}
            if (figure2.equals("No File Selected")) {figure2 = filepath;}
            if (figure3.equals("No File Selected")) {figure3 = filepath;}
            });



        //FOR OBSTACLE SELECTION
        Label normObsText = new Label("Enter number of normal obstacles");
        normObsText.setPrefWidth(300);
        normObsText.setAlignment(Pos.CENTER);
        normObsText.getStyleClass().add("check-button");
        Spinner<Integer> normalObstInt = new Spinner<>(0, 100, 0, 1); // Spinner with range 0-100, initial value 0, step 1
        normalObstInt.setEditable(true); // Allows user to type a number

        //Gets value from spinner
        normalObstInt.valueProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue != null) {
                normalObstacles = newValue;
            }
        });

        Label gameObsText = new Label("Enter number of game obstacles");
        gameObsText.setPrefWidth(300);
        gameObsText.setAlignment(Pos.CENTER);
        gameObsText.getStyleClass().add("check-button");
        Spinner<Integer> gameObstInt = new Spinner<>(0, 100, 0, 1); // Spinner with range 0-100, initial value 0, step 1
        gameObstInt.setEditable(true); // Allows user to type a number

        //Gets value from spinner
        gameObstInt.valueProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue != null) {
                gameObstacles = newValue;
            }
        });



        Button startButton = new Button("Start Simulation");
        startButton.getStyleClass().add("menu-button");
        startButton.setOnAction(event -> {
            GraphicsAndWindowsTest GWT = new GraphicsAndWindowsTest(getParameters().getRaw().toArray(new String[0]),
                    figure1, figure2, figure3, normalObstacles, gameObstacles);
            //GraphicsAndWindowsTest GWT = new GraphicsAndWindowsTest();
            try {
                GWT.start(stage);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        //PARENT OF ALL THE LAYOUTS
        VBox menuLayout = new VBox(10);
        menuLayout.setAlignment(Pos.CENTER);
        menuLayout.getChildren().addAll(startButton, figHBox,selector, FPButton, selectedFileLabel,normObsText,
                normalObstInt, gameObsText, gameObstInt);

        Scene menuScene = new Scene(menuLayout, 1500, 800);
        menuScene.getStylesheets().add(getClass().getResource("/MainMenu.css").toExternalForm());
        return menuScene;
    }

    /**
     * The main entry point for the Java application.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {launch(args);}

    /**
     * Determines the file path based on the selected radio button.
     *
     * @param selected The selected radio button.
     * @return The file path corresponding to the selected radio button.
     */
    public String chooseFigureStr(RadioButton selected){
        return switch (selected.getText()) {
            case "Armadillo" -> "src/armadillo.txt";
            case "Teapot" -> "src/teapot.txt";
            case "Sphere" -> "src/sphereOut.txt";
            case "None" -> "none";
            case "Command Line" -> "Use CL";
            case "File Path" -> filepath;
            default -> "nothing picked";
        };
    }
}

